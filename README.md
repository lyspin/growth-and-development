# Growth and development

This is a personal project to help track my growth and development.

I use it to track:

- [Career development](https://handbook.gitlab.com/handbook/people-group/learning-and-development/career-development)
- Alignment with [GitLab Values](https://handbook.gitlab.com/handbook/values/)
- Noteworthy achievements
- Weekly progress on things I work on
- Bi-weekly mentorship progress

## Tracking issues

The tracking issues are created using:

- [GitLab API](https://docs.gitlab.com/ee/api/rest/index.html)
- [GitLab CLI - `glab`](https://docs.gitlab.com/ee/editor_extensions/gitlab_cli/)
- [Pipeline schedules](https://docs.gitlab.com/ee/ci/pipelines/schedules.html)
- [Issue templates](https://docs.gitlab.com/ee/user/project/description_templates.html#create-an-issue-template) with [quick actions](https://docs.gitlab.com/ee/user/project/quick_actions.html#issues-merge-requests-and-epics) to:
  - Assign the issue to me.
  - Make the issue confidential.
  - Add a label.
  - Set a due date.
  - Create a to-do.

### Weekly tracking 

A scheduled pipeline is used to automatically create the weekly tracking issue every Monday at 9am Eastern Time.

The [weekly tracking](https://gitlab.com/lyspin/growth-and-development/-/blob/main/issue_templates/weekly_tracking.md?ref_type=heads) issue helps track:

- Planned tasks and goals
- Retrospective reflections including:
  - Achievements and celebrations
  - Areas for improvement
- Growth opportunities and learning goals

### Mentorship progress

A separate scheduled pipeline automatically creates a bi-weekly tracking issue every other Monday at 9am Eastern Time.

The [mentorship progress](https://gitlab.com/lyspin/growth-and-development/-/blob/main/issue_templates/mentorship_progress.md?ref_type=heads) issue complements bi-weekly mentor meetings for the [GitLab Mentorship program hosted by the Women TMRG](https://handbook.gitlab.com/handbook/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/) and helps track:

- Goals review and progress.
- Discussion points from mentorship sessions.
- Action items and next steps.
- Key learnings and resources shared.
