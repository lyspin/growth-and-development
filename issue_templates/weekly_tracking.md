
# Weekly tracking

This template helps track and reflect on weekly progress, ensuring consistent growth and improvement through structured planning and retrospection.

<!-- Use it to set clear goals, celebrate wins, identify areas for improvement, and capture learning opportunities. -->

## 📋 Plan

<!-- List key objectives and tasks for the week. Keep it focused on 3-5 main priorities. -->

- [ ]
- [ ]
- [ ]

## 💭 Retrospective

<!-- Reflections on the week's experiences and outcomes to learn and improve. -->

### 🎉 To celebrate

<!-- Acknowledge wins, accomplishments, and positive moments from the week. -->

- 

### 🚧 To improve

<!-- Note areas where things could have gone better or processes that need adjustment. -->

- 

## 🌱 Opportunities to grow

<!-- Identify skills to develop, lessons learned, and potential areas for personal/professional development. -->

- 

/confidential
/assign @lyspin
/label ~weekly
/due in 5 days
/todo