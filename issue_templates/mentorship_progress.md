
# Mentorship progress

This template helps track progress in the [GitLab Mentorship program hosted by the Women TMRG](https://handbook.gitlab.com/handbook/company/culture/inclusion/tmrg-gitlab-women/mentorship-program/), a 5-month career development opportunity.

## 🎯 Goals and progress

| Goal | Status | Actions taken | Next steps |
|------|--------|----------------|------------|
| 1. [Goal description] | 🟢 On track / 🟡 At risk / 🔴 Blocked | - [ ] Action 1<br>- [ ] Action 2 | - [ ] Next step 1<br>- [ ] Next step 2 |
| 2. [Goal description] | | | |
| 3. [Goal description] | | | |


## 💬 Discussion topics

- 
- 
- 

## ✨ Key takeaways

- 
- 
- 

## 📚 Resources

- 

/confidential
/assign @lyspin
/label ~mentorship
/due in 2 weeks
/todo